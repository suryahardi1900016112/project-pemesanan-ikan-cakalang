<?php
class Detail_paket_model extends CI_Model{
    public $id_produk;
    public $id_paket;
    public $created_at;
    public $updated_at;

    public function getByPackage()
    {
        $this->load->database();
        $detailpaket = $this->db->get("Detail_paket");
        $result = $detailpaket->result();
        return json_encode($result);
    }
}